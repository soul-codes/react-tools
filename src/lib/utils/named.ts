/**
 * Adds a display name property to a React entity (context, component, etc.)
 * This method mutates the input object's `displayName` property and returns the
 * same object back.
 */
export const named = (name: string) => <T extends { displayName?: string }>(
  object: T
): T => ((object.displayName = name), object);
