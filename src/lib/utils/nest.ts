import { ReactElement, ReactNode, cloneElement } from "react";

/**
 * Create a function that wraps a valid React node as child of the given React
 * element. Useful in a pipeline pattern where a node is wrapped in e.g. multiple
 * context providers.
 * @param el
 */
export const nest = (el: ReactElement) => (child: ReactNode) =>
  cloneElement(el, {}, child);
