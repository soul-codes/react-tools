import { Comparer } from "fp/comparer";

/**
 * Configures the global default structural comparer for react-tools. This
 * comparer will be used as fallback in places where a structural comparison
 * function is required but not explicitly configured.
 * @param comparer
 */
export const setDefaultComparer = (comparer: Comparer<unknown>) =>
  (configuredComparer = comparer);

export const defaultComparer: Comparer<unknown> = (a, b) =>
  configuredComparer(a, b);

export const referentialComparer: Comparer<unknown> = (a, b) => a === b;

/**
 * @deprecated use `referentialComparer` (with the correct spelling).
 */
export const referentialCOmparer = referentialComparer;

const libraryDefaultComparer: Comparer<unknown> = (a, b) =>
  requireLodashIsEqual()(a, b);

let configuredComparer = libraryDefaultComparer;

const requireLodashIsEqual = (() => {
  let isEqual: Comparer<unknown> | null = null;
  return () => {
    if (isEqual) return isEqual;
    try {
      const module = require("lodash/isEqual").default;
      if (typeof module !== "function")
        throw Error("Expected lodash isEqual() to be a function.");
      return (isEqual = module);
    } catch (error) {
      throw Error(
        "react-tools uses lodash/isEqual as the default structural comparer, but this module cannot be found. Make sure you have lodash/isEqual installed, otherwise configure an alternative default structural comparer function. Require error: " +
          String(error)
      );
    }
  };
})();
