import { Maybe } from "fp/maybe";

export const once = <T extends (...args: any) => any>(fn: T): T => {
  let lastResult: Maybe<any> = null;
  return ((...args: any) =>
    lastResult
      ? lastResult.value
      : (lastResult = { value: fn(...args) }).value) as T;
};
