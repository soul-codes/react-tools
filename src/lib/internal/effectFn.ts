import { useEffect, useLayoutEffect } from "react";

export function effectFn(layout: boolean) {
  return layout ? useLayoutEffect : useEffect;
}
