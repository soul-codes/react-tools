export function depsOrFn(
  deps: readonly unknown[] | undefined,
  fn: (...args: any) => any
) {
  return deps ? [true, ...deps] : [false, fn];
}
