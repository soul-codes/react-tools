import { Dispatch } from "fp/dispatch";

export interface Disposer {
  (): void;
}

export interface CallbackSubscriber<T> {
  (cb: Dispatch<T>): Disposer;
}
