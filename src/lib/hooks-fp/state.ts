import { reconcile } from "accumulators";
import { Comparer } from "fp/comparer";
import { Dispatch } from "fp/dispatch";
import { Map } from "fp/map";
import * as t2 from "fp/tuple2";
import { useForcedUpdate } from "hooks/useForcedUpdate";
import { useInit } from "hooks/useInit";
import { defaultComparer } from "internal/comparer";
import { noop } from "internal/noop";
import { useRef, useState } from "react";

export interface StateHook<T, D, U> {
  (defaultValue: U): t2.Tuple2<T, Dispatch<D>>;
}

/**
 * Create a typed `useState` hook where the dispatch and value types are the
 * same.
 */
export const of = <T>(): StateHook<T, T, T> => useState;

/**
 * Creates a function that transforms the state value of a state/dispatch tuple.
 * @param fn
 */
export const map = <A, B>(fn: Map<A, B>) => <D, U>(
  m: StateHook<A, D, U>
): StateHook<B, D, U> => (defaultValue) => t2.map0(fn)(m(defaultValue));

/**
 * Creates a function that pre-transforms the dispatch value of a state/dispatch
 * tuple.
 * @param fn
 */
export const mapDispatch = <A, B>(fn: Map<B, A>) => <T, U>(
  m: StateHook<T, A, U>
): StateHook<T, B, U> => (defaultValue) =>
  t2.map1(
    (dispatch: Dispatch<A>): Dispatch<B> =>
      useInit(() => (value) => dispatch(fn(value)))
  )(m(defaultValue));

/**
 * Creates a function that pre-transforms the dispatch value of a state/dispatch
 * tuple by making it a reducer based on the current state value.
 * @param fn
 */
export const mapReducer = <A, B, T>(fn: Map<B, Map<T, A>>) => <U>(
  m: StateHook<T, A, U>
): StateHook<T, B, U> => (defaultValue) => {
  const [value, dispatch] = m(defaultValue);
  const valueRef = useRef(value);
  valueRef.current = value;
  return [
    value,
    useInit(() => (payload) => dispatch(fn(payload)(valueRef.current))),
  ];
};

/**
 * Creates a function that pre-transforms default value of the `useState` hook.
 * @param fn
 */
export const mapDefault = <A, B>(fn: Map<B, A>) => <T, D>(
  m: StateHook<T, D, A>
): StateHook<T, D, B> => (defaultValue) => m(fn(defaultValue));

/**
 * Creates a function that referentially stabilizes the value of the
 * state/dispatch tuple based on the comparer.
 * @param comparer
 */
export const struct = <T>(comparer: Comparer<T> = defaultComparer) =>
  map(reconcile(comparer));

/**
 * Creates a function that referentially pre-stabilizes the dispatch payload of the
 * state/dispatch tuple based on the comparer.
 * @param comparer
 */
export const structDispatch = <T>(comparer: Comparer<T> = defaultComparer) =>
  mapDispatch(reconcile(comparer));

/**
 * Creates a state hook factory, but allowing the state to be dynamically
 * controlled.
 *
 * @param value The controlled state value. If this value is `undefined`, then
 * the state is uncontrolled and the resulting hook behaves like a normal
 * `useState` hook. Otherwise, the state is controlled and the resulting hook
 * is a simple proxy for `value` itself.
 *
 * @param dispatch The callback to be called when the state changed. This will
 * be called upon state change regardless of whether the state is controlled or
 * not. If the dispatch isn't given, default dispatch behavior will kick in.
 */
export const controllable = <T>(
  value?: T,
  dispatch?: Dispatch<T>,
  options?: {
    /**
     * If `true`, a `dispatch fallback` behavior will kick in, where the
     * component using controlled state will refresh. Default is `true` if
     * the state is controlled (`value` is `undefined`) and `false` otherwise.
     */
    dispatchFallback?: boolean;
  }
): StateHook<T, T, T> => (initialValue: T) => {
  const ref = useRef({
    value: initialValue,
    isControlled: false,
    dispatch,
    dispatchFallback: false,
  });
  const refresh = useForcedUpdate();
  ref.current.isControlled = value !== void 0;
  ref.current.dispatch = dispatch;
  ref.current.dispatchFallback =
    options?.dispatchFallback ?? ref.current.isControlled;
  if (value !== void 0) ref.current.value = value;

  const setState = useInit(() => (value: T) => {
    !ref.current.isControlled && (ref.current.value = value);
    const dispatch =
      ref.current.dispatch || (ref.current.dispatchFallback ? refresh : noop);
    dispatch(value);
  });
  return [ref.current.value, setState];
};

/**
 * A convenient type for generating from a prop interface a corresponding type
 * of controllable states, with each prop made optional and corresponding
 * `on<PropName>Changed` dispatch type for each prop type.
 */
export type ControllableState<T> = Partial<T> &
  UnionToIntersection<
    {
      [prop in keyof T & string]: {
        [dispatchProp in `on${Capitalize<prop>}Change`]?: Dispatch<T[prop]>;
      };
    }[keyof T & string]
  >;

type UnionToIntersection<T> = (T extends any ? (x: T) => any : never) extends (
  x: infer R
) => any
  ? R
  : never;

/**
 * A pipe-convenience method to invoke the `useState` hook.
 * @param comparer
 */
export const use = <U>(defaultValue: U) => <T, D>(m: StateHook<T, D, U>) =>
  m(defaultValue);
