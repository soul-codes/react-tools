import { Comparer } from "fp/comparer";
import { Map } from "fp/map";
import { pipe } from "fp/pipe";
import { useReconciler } from "hooks/useReconciler";
import { defaultComparer } from "internal/comparer";
import { Context, useContext } from "react";

/**
 * Create a context consumer hook from a context.
 * @param context
 */
export const of = <T>(context: Context<T>): ContextHook<T> => () =>
  useContext(context);

/**
 * Returns a function that can be used to transform the value of a context
 * consumer hook
 * @param fn
 * @param deps
 */
export const map = <A, B>(fn: Map<A, B>) => (
  m: ContextHook<A>
): ContextHook<B> => () => fn(m());

/**
 * Returns a context consumer that merges together the values from other
 * context consumers in a structure.
 * @param struct
 */
export const sequenceS = <T extends Record<string, any>>(
  struct: { [key in keyof T]: ContextHook<T[key]> }
): ContextHook<T> => () =>
  Object.fromEntries(
    Object.entries(struct).map(([key, useContext]) => [key, useContext()])
  ) as T;

/**
 * Returns a context consumer that merges together the values from other
 * context consumers in a tuple.
 * @param struct
 */
export const sequenceT = <T extends any[]>(
  tuple: { [key in keyof T]: ContextHook<T[key]> }
): ContextHook<T> => () => tuple.map((useContext) => useContext()) as T;

/**
 * Make the result of a context consumer referentially stable against the
 * previous value if possible, based on the given comparer.
 * @param comparer
 */
export const struct = <T>(comparer: Comparer<T> = defaultComparer) => (
  m: ContextHook<T>
) => () => pipe(m(), useReconciler(comparer));

/**
 * Pipe-convenience method to evaluate the context hook.
 * @param m
 *
 */
export const use = <T>(m: ContextHook<T>) => m();

export interface ContextHook<T> {
  (): T;
}
