import { depsOrFn } from "internal/depsOrFn";
import { effectFn } from "internal/effectFn";
import React, { Fragment, ReactNode } from "react";

/**
 * An effect component. This has an advantage over using the hooks directly in
 * that the effects can be re-ordered or made conditional, thanks to the
 * component-level isolation of hook ordering.
 *
 * The component also doubles as a pass-through component for rendering children
 * @param props
 */
export function Effect(props: EffectProps) {
  effectFn(props.layout ?? false)(
    props.fn,
    props.always ? [{}] : depsOrFn(props.deps, props.fn)
  );
  return props.children != null && props.children !== false ? (
    <Fragment>{props.children}</Fragment>
  ) : null;
}

export interface EffectProps {
  fn: () => Disposer | void;
  layout?: boolean;
  deps?: readonly unknown[];
  always?: boolean;
  children?: ReactNode;
}

export interface Disposer {
  (): void;
}
