import { useInit } from "hooks/index";
import React, { Fragment, ReactNode } from "react";
import { createPortal } from "react-dom";
import {
  InPortal,
  OutPortal,
  createHtmlPortalNode,
} from "react-reverse-portal";

/**
 * The default suspense mask component that renders the mask content visibly
 * while rendering the real contents in a hidden (and detached) portal until
 * there is no mask content, where the real content is attached to the DOM
 * tree.
 * @param props
 */
export function SuspenseMask(props: SuspenseMaskProps) {
  const reversePortalNode = useInit(() => createHtmlPortalNode());
  const hiddenPortalNode = useInit(() => document.createElement("div"));
  return (
    <Fragment>
      <InPortal node={reversePortalNode}>{props.children}</InPortal>
      {props.mask}
      {props.mask ? (
        createPortal(<OutPortal node={reversePortalNode} />, hiddenPortalNode)
      ) : (
        <OutPortal node={reversePortalNode} />
      )}
    </Fragment>
  );
}

export interface SuspenseMaskProps {
  children?: ReactNode;
  mask?: ReactNode;
}
