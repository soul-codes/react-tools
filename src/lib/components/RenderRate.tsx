import { timeout } from "effects/timeout";
import { pipe } from "fp/pipe";
import * as t from "fp/task";
import { useEffectRefs, useInit } from "hooks/index";
import { Collect, collect } from "patterns/collect";
import React, { Fragment, useEffect } from "react";

/**
 * A component that shows the render rate (re-renders per second). Attach this
 * component as a child of an element whose rerendering is to be measured.
 */
export function RenderRate() {
  const countState = useInit(() => collect(0));
  const [ref] = useEffectRefs(
    (el: HTMLDivElement | null) => {
      if (!el) return;
      el.style.opacity = "1";
      el.style.transition = "";
      el.style.transitionDelay = "";
      return timeout(() => {
        el.style.transition = "opacity 0.4s";
        el.style.opacity = "0";
      }, 1000);
    },
    [{}]
  );

  useEffect(() => {
    pipe(
      () => new Promise<void>((resolve) => setTimeout(resolve, 1000)),
      t.wrap(() => countState.reduceEffect((n) => ++n))
    )();
  }, [{}]);

  return (
    <div
      ref={ref}
      style={{
        position: "absolute",
        width: 12,
        height: 12,
        fontSize: 12,
        color: "red",
        backgroundColor: "rgba(255,255,255,0.5)",
        borderRadius: 6,
        textAlign: "center",
      }}
    >
      <Count state={countState} />
    </div>
  );
}

function Count(props: { state: Collect<number> }) {
  return <Fragment>{props.state.useValue()}</Fragment>;
}
