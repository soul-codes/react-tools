import { Disposer } from "internal/@types";

/**
 * Convenient disposable version of `requestAnimationFrame`.
 * @param fn
 */
export function animationFrame(fn: () => void): Disposer {
  const handle = requestAnimationFrame(fn);
  return () => cancelAnimationFrame(handle);
}
