import { Disposer } from "internal/@types";

/**
 * Convenient disposable version of `setInterval`
 * @param fn
 * @param ms
 */
export function interval(fn: () => void, ms: number): Disposer {
  const int = setInterval(fn, ms);
  return () => clearInterval(int);
}
