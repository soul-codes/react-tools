/// <reference types="resize-observer-browser" />

import { Dispatch } from "fp/dispatch";

/**
 * Convenient disposable version of a resize observer.
 * @param el
 * @param cb
 */
export function resizeObserve(el: Element, cb: Dispatch<void>) {
  const ro = new ResizeObserver(() => cb());
  ro.observe(el);
  return () => ro.disconnect();
}
