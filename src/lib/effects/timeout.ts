import { Disposer } from "internal/@types";

/**
 * Convenient disposable version of `setTimeout`.
 * @param fn
 * @param ms
 */
export function timeout(fn: () => void, ms: number): Disposer {
  const timeout = setTimeout(fn, ms);
  return () => clearTimeout(timeout);
}
