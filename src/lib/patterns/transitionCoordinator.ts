import { atomicEvent } from "atomic-event";
import { Dispatch } from "fp/dispatch";
import { useForcedUpdate, useInit } from "hooks/index";
import { CallbackSubscriber, Disposer } from "internal/@types";
import { useEffect, useRef } from "react";
import { reffx } from "reffx";

/**
 * Create a transition coordinator instance.
 *
 * A transition coordinator is used to "throttle" changes presentation values so
 * that they all happen in a coordinated manner. The coordination is achieved
 * by using "locks". Various processes in the UI can request a lock on the
 * coordinator. Rendering value changes will be buffered while a non-zero number
 * of locks exists on the coordinator. The changes are finally "flushed" all
 * locks are released.
 */
export function transitionCoordinator(): TransitionCoordinator {
  let isLocked = false;
  const [listen, emit] = atomicEvent<boolean>();

  const lock = reffx(() => {
    isLocked = true;
    emit(true);
    return () => {
      isLocked = false;
      emit(false);
    };
  });

  return {
    lock,
    useCoordinatedTransition: (value) => {
      const subscribeLock = useInit(() => (cb: Dispatch<boolean>) => {
        const unlisten = listen(cb);
        cb(isLocked);
        return unlisten;
      });
      return useCoordinatedTransition(subscribeLock, value);
    },
  };
}

function useCoordinatedTransition<T>(
  subscribeLock: CallbackSubscriber<boolean>,
  value: T
) {
  const state = useRef({
    isLocked: false,
    lockedValue: value,
    freshValue: value,
  }).current;
  state.freshValue = value;
  const update = useForcedUpdate();

  useEffect(
    () =>
      subscribeLock((isLocked) => {
        if (state.isLocked && !isLocked) update();
        if (!state.isLocked && isLocked) state.lockedValue = state.freshValue;
        state.isLocked = isLocked;
      }),
    [subscribeLock]
  );
  return state.isLocked ? state.lockedValue : state.freshValue;
}

export interface TransitionCoordinator {
  /**
   * Adds a lock on the current transition. The disposer is used to remove that
   * lock.
   */
  lock(): Disposer;

  /**
   * The hook returns coordinated transition value for the new state value.
   * This returns the same value as the input if the coordinate is not in a
   * locked state.
   * @param value
   */
  useCoordinatedTransition<T>(value: T): T;
}
