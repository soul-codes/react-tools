import { tuple } from "fp-ts/lib/function.js";
import { Comparer } from "fp/comparer";
import { StateHook } from "hooks-fp/state";
import { useForcedUpdate } from "hooks/index";
import { defaultComparer } from "internal/comparer";
import { Dispatch, useEffect } from "react";

/**
 * Create a state instance that is independent of any react component instance
 * lifecycle. The instance can then be called like a `useState` hook.
 */
export function state<T>(
  initial: T,
  options: { comparer?: Comparer<T> } = {}
): StateInstance<T> {
  const comparer = options?.comparer ?? defaultComparer;
  const listeners = new Set<Dispatch<T>>();
  let stateValue = initial;

  function handleChange() {
    const v = stateValue;
    for (const cb of listeners) cb(v);
  }

  const useValue = () => {
    const refresh = useForcedUpdate();
    listeners.add(refresh);
    useEffect(() => () => void listeners.delete(refresh), []);
    return stateValue;
  };

  const setValue = (value: T) => {
    if (comparer(value, stateValue)) return;
    stateValue = value;
    handleChange();
  };

  return Object.assign(() => tuple(useValue(), setValue), {
    value: () => stateValue,
  });
}

export interface StateInstance<T> extends StateHook<T, T, void> {
  value(): T;
}
