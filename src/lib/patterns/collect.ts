import { diff } from "accumulators";
import { Disposer } from "components/Effect";
import { Comparer } from "fp/comparer";
import { Map } from "fp/map";
import { Maybe } from "fp/maybe";
import { pipe } from "fp/pipe";
import * as st from "hooks-fp/state";
import { useInit } from "hooks/index";
import { defaultComparer } from "internal/comparer";
import { depsOrFn } from "internal/depsOrFn";
import { effectFn } from "internal/effectFn";
import { once } from "internal/once";
import { Dispatch, useCallback, useEffect, useRef } from "react";

import { identity } from "../internal/identity";

/**
 * Create a state reduction-collector.
 *
 * A state reduction-collector is a state management mechanism whereby the
 * value of the state is described by an initial value and a number of reducer
 * instances that are instantiated, updated and destroyed throughout the component
 * lifecycle.
 *
 * A state that can be managed in this way must be able to be expressed as
 *
 * ```
 * current_value = initial_value |> reducer1 |> reducer2 |> ... |> reducerN
 * ```
 *
 * Where `|>` denotes the piping operator, and where `reducer*` denotes
 * registered reducer instances.
 *
 * Reducer instances can be created as effect hooks, as refs, and as one-off
 * disposable effect. The most important rule about reducer instances is that
 * the complete set of reducers must be associative, that is, the result of the
 * reduction must be the same regardless of the order in which the reducers are
 * applied.
 *
 * @param initial
 * @param options
 */
export function collect<T>(
  initial: T,
  options: { async?: boolean; comparer?: Comparer<T> } = {}
): Collect<T> {
  const listeners = new Set<Dispatch<T>>();
  const reducers = new Map<object, Map<T, T>>();
  const layout = !(options.async ?? false);
  const scope = {};
  let batchedHandleChange = once(handleChange);

  function handleChange() {
    batchedHandleChange = once(handleChange);
    const value = [...reducers.values()].reduce(
      (reducee, reducer) => reducer(reducee),
      initial
    );
    for (const cb of listeners) cb(value);
  }

  const useProjection = function <U>(
    fn: Map<T, U>,
    deps?: unknown[],
    options?: { comparer?: Comparer<U> }
  ) {
    const [value, setValue] = pipe(
      st.of<U>(),
      st.structDispatch(options?.comparer),
      st.mapDispatch(useCallback(fn, depsOrFn(deps, fn))),
      st.use(fn(initial))
    );

    listeners.add(setValue);
    useEffect(() => () => void listeners.delete(setValue), [scope]);
    return value;
  };

  const useValue = () => useProjection(identity);

  const useReducer = (reducer: Map<T, T>, deps?: unknown[]) => {
    const { current: token } = useRef({});
    reducers.set(token, reducer);

    effectFn(layout)(() => batchedHandleChange(), [
      scope,
      ...depsOrFn(deps, reducer),
    ]);
    effectFn(layout)(() => () => void reducers.delete(token), [scope]);
  };

  const useReducerRef = <R>(
    reducer: (ref: R) => Map<T, T>,
    deps?: unknown[],
    options?: { comparer?: Comparer<R | null> }
  ): Dispatch<R | null> => {
    const token = useRef<object | null>({});
    const getChangedValue = useInit(() =>
      diff<R | null, Maybe<R | null>>(
        (cur, prev) =>
          (options?.comparer ?? defaultComparer)(cur, prev)
            ? null
            : { value: cur },
        null
      )
    );

    useEffect(
      () => () => {
        const lastToken = token.current;
        token.current = null;
        if (lastToken && reducers.get(lastToken)) {
          reducers.delete(lastToken);
          batchedHandleChange();
        }
      },
      []
    );

    return useCallback((ref: R | null) => {
      const delta = getChangedValue(ref);
      if (!delta || !token.current) return;

      if (delta.value == null) {
        reducers.delete(token.current);
      } else {
        reducers.set(token.current, reducer(delta.value));
      }
      batchedHandleChange();
    }, depsOrFn(deps, reducer));
  };

  const reduceEffect = (reducer: Map<T, T>) => {
    const token = {};
    reducers.set(token, reducer);
    batchedHandleChange();
    return () => {
      reducers.delete(token);
      batchedHandleChange();
    };
  };

  return {
    useValue,
    useReducer,
    useProjection,
    useReducerRef,
    reduceEffect,
  };
}

export interface Collect<T> {
  /**
   * A hook to obtain the value of the reducer/collector state, refreshing the
   * component if the value changes according to the comparer.
   */
  useValue(): T;

  /**
   * A hook to obtain the value of the reducer/collector state, refreshing
   * the component if the projection value changes according to the comparer.
   * @param fn
   * @param deps
   */
  useProjection<U>(
    fn: Map<T, U>,
    deps?: unknown[],
    options?: { comparer?: Comparer<U> }
  ): U;

  /**
   * A hook that suplies a reducer instance to the reduction-collector until the
   * component unmounts.
   * @param reducer
   * @param deps
   */
  useReducer(reducer: Map<T, T>, deps?: unknown[]): void;

  /**
   * A hook that returns a ref that can be called to refresh the
   * reduction-collector with a reducer instance until the component unmounts
   * or until the ref function is invoked with `null`.
   * @param reducer
   * @param deps
   * @param options
   */
  useReducerRef<R>(
    reducer: Map<R, Map<T, T>>,
    deps?: unknown[],
    options?: { comparer?: Comparer<R | null> }
  ): Dispatch<R | null>;

  /**
   * Instantiates a reducer for the reduction-collector until the disposer is
   * called. This is *not* a hook method; rather, it is intended to be used inside
   * an effect callback, and its resulting disposer should be invoked as part
   * of the effect cleanup.
   * @param reducer
   */
  reduceEffect(reducer: Map<T, T>): Disposer;
}
