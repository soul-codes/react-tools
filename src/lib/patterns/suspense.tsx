import { SuspenseMask, SuspenseMaskProps } from "components/SuspenseMask";
import { pipe } from "fp-ts/lib/function.js";
import { Comparer } from "fp/comparer";
import { Map as MapFn } from "fp/map";
import { Tuple2 } from "fp/tuple2";
import { useInit } from "hooks/index";
import { useReconciler } from "hooks/useReconciler";
import { defaultComparer } from "internal/comparer";
import { identity } from "internal/identity";
import React, { ComponentType, createContext, useContext, useRef } from "react";

import { Collect, collect } from "./collect";

/**
 * Creates a suspense manager.
 * @param reducer
 * @param options
 * @template L Type of the leaf suspense state
 * @template B Type of the branch suspense state
 * @template P Type fo the suspense layout component props
 */
export function suspense<L, B = L, P = SuspenseMaskProps>(
  reducer: (states: (L | B)[]) => B,
  options?: SuspenseOptions<L, B, P>
): SuspenseManager<L, B, P> {
  const SuspenseLayout = options?.layout ?? SuspenseMask;
  const internalSuspenseContext = createContext<Collect<
    Map<object, L | B>
  > | null>(null);

  function useSuspense(transform: MapFn<B, B> = identity) {
    const childStates = useInit(() => collect(new Map<object, L | B>()));
    const branchState = pipe(
      childStates.useProjection((vals) => reducer([...vals.values()])),
      useReconciler()
    );
    useReport(transform(branchState));
    const Provider = useInit(() => (props: P) => (
      <internalSuspenseContext.Provider value={childStates}>
        <SuspenseLayout {...props} />
      </internalSuspenseContext.Provider>
    ));

    return [branchState, Provider] as const;
  }

  function Report(props: { state: L | B }) {
    useReport(props.state);
    return null;
  }

  function useReport(status: L | B) {
    status = pipe(status, useReconciler(options?.comparer ?? defaultComparer));
    const { current: token } = useRef({});
    const loadings = useContext(internalSuspenseContext);
    loadings?.useReducer(
      (value) => pipe(new Map(value), (map) => map.set(token, status)),
      [status]
    );
  }

  return {
    useSuspense,
    useReport,
    Report,
  };
}

export interface SuspenseManager<L, B, P> {
  /**
   * A hook returning a tuple of suspense state and the suspense provider
   * component. The former is can be used to determine how to present the
   * UI based on the current suspense state, and any component that is
   * contributing suspense states must be wrapped within the suspense provider.
   * @param transform
   */
  useSuspense(transform?: MapFn<B, B>): Tuple2<B, ComponentType<P>>;

  /**
   * An effect hook that reports the given suspense state to the upstream
   * components.
   * @param status
   */
  useReport(status: L | B): void;

  /**
   * A convenient component version of `useReport`.
   */
  Report: ComponentType<{ state: L | B }>;
}

export interface SuspenseOptions<T, U, P = SuspenseMaskProps> {
  /**
   * The optional comparer used to referentially stabilize leaf and Branch
   * suspense states.
   */
  comparer?: Comparer<T | U>;

  /**
   * The suspense layout component that should be "baked in" to the suspense
   * manager.
   */
  layout?: ComponentType<P>;
}
