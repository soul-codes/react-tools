import { useRef } from "react";

import { isDepEqual } from "../internal/isDepEqual";

/**
 * Like `useMemo()`, but ensuring the semantic guarantee that referntially equal
 * dependencies will give rise to a referentially equal value as long as the
 * equality holds between subsequent calls of this hook.
 */
export function useMemoS<T>(value: () => T, deps?: unknown[]): T {
  const storeRef = useRef<{ value: T; deps: unknown[] } | null>(null);
  if (!storeRef.current || !isDepEqual(deps || [], storeRef.current.deps)) {
    storeRef.current = { value: value(), deps: deps || [] };
  }
  return storeRef!.current.value;
}
