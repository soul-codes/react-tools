import { NonFunction } from "fp/function";
import { useState } from "react";

/**
 * Returns a constant that is initialized only once during the entire component's
 * lifecycle.
 * @param initializer
 */
export function useInit<T>(initializer: (() => T) | NonFunction<T>): T {
  return useState(initializer)[0];
}
