import { MutableRefObject, useRef } from "react";

/**
 * A convenient hook for creating a ref whose value is always assigned to the
 * given value at every rendering pass.
 * @param value
 */
export function useLeadingRef<T>(value: T): Readonly<MutableRefObject<T>> {
  const ref = useRef(value);
  ref.current = value;
  return ref;
}
