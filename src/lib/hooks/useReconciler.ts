import { reconcile } from "accumulators";
import { Comparer } from "fp/comparer";
import { Map } from "fp/map";
import { defaultComparer } from "internal/comparer";

import { useInit } from "./useInit";

/**
 * Creates a hooked reconciler function that returns the referentially identical
 * value as the value at last render if they are deemed equal by the comparer
 */
export function useReconciler<T>(
  comparer: Comparer<T> = defaultComparer
): Map<T, T> {
  return useInit(() => reconcile(comparer));
}
