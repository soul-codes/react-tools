import { Dispatch } from "fp/dispatch";

import { resizeObserve } from "../effects/resizeObserve";
import { useEffectRefs } from "./useEffectRefs";

/**
 * Creates a hook that notifies the callback of the element when its size
 * changes as determined by the resize observer.
 * @param cb
 */
export function useResizeObserverRef<E extends Element>(cb: Dispatch<E>) {
  return useEffectRefs(
    (el: E | null) =>
      el
        ? (cb(el), resizeObserve(el, () => requestAnimationFrame(() => cb(el))))
        : void 0,
    [cb],
    { layout: true } // Make this a React Layout Effect to prevent flickering.
  )[0];
}
