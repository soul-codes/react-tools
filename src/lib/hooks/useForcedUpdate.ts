import { useState } from "react";

import { useInit } from "./useInit";

/**
 * A hook that returns a function that can be called to force the component to
 * update. `useState` is generally a better way to do this, but this hook helps
 * in the case where it makes more sense to maintain a state manually using
 * refs.
 */
export function useForcedUpdate() {
  const [, setState] = useState({});
  return useInit(() => () => setState({}));
}
