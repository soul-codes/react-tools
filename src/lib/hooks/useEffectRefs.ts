import { pipe } from "fp/pipe";
import { Disposer } from "internal/@types";
import { effectFn } from "internal/effectFn";
import { collect } from "patterns/collect";

import { useInit } from "./useInit";
import { useReconciler } from "./useReconciler";

/**
 * A React hook that applies an effect to a set of ref values. Returns series of
 * corresponding ref function for each argument. The callback function will be
 * called only once per re-render even if several refs change.
 *
 * Note that the arity of the effect callback (either deduced by the `length`
 * property or specifically given in `options.forceRefCount`) is expected to be
 * the same each time the hooks is used. Later changes will silently ignored.
 *
 * @param cb Can optionally return a disposer for when the ref changes.
 */
export function useEffectRefs<T extends any[]>(
  cb: (...els: T) => void | Disposer,
  deps?: unknown[],
  options: EffectRefOptions = {}
): EffectRefs<T> {
  const collectEls = useInit(() => collect<unknown[]>([]));
  const refs = new Array(useInit(() => options.forceRefCount ?? cb.length ?? 0))
    .fill(0)
    .map((_, index) =>
      collectEls.useReducerRef(
        (el) => (els) => pipe([...els], (els) => ((els[index] = el), els)),
        [index]
      )
    ) as EffectRefs<T>;

  const els = pipe(collectEls.useValue() as T, useReconciler(isRefEqual));
  effectFn(options.layout ?? false)(() => cb(...els), [els, ...(deps ?? [])]);
  return refs;
}

export interface EffectRefOptions {
  /**
   * If true then the effect should be a layout effect.
   */
  layout?: boolean;

  /**
   * In case the number refs cannot be determined from the callback function
   * (e.g. because the callback accepts a variadic number of arguments), this
   * option forces the ref count for each time the callback is being given.
   */
  forceRefCount?: number;
}

export type EffectRefs<T extends any[]> = {
  [index in keyof T]: (el: T[index]) => void;
};

function isRefEqual<T extends unknown[]>(a: T, b: T) {
  if (a.length !== b.length) return false;
  for (let i = 0, length = a.length; i < length; i++) {
    const aa = a[i];
    const bb = b[i];
    if (!!aa !== !!bb) return false;
  }
  return true;
}
