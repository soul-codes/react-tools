import { setDefaultComparer } from "internal/comparer";

export const configureDefaultComparer = setDefaultComparer;
