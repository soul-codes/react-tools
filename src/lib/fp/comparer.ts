export interface Comparer<T> {
  (a: T, b: T): boolean;
}

export const pre = <A, B>(fn: Precomparer<A, B>) => (
  comparer: Comparer<B>
): Comparer<A> => (a, b) => fn(a, b, comparer);

export type Precomparer<A, B> = (a: A, b: A, compare: Comparer<B>) => boolean;
