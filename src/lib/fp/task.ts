import { Task } from "fp-ts/lib/Task.js";
import { Disposer } from "internal/@types";

import { pipe } from "./pipe";

export const wrap = (effect: () => Disposer) => <T>(
  task: Task<T>
): Task<T> => () => pipe(effect(), (dispose) => task().finally(dispose));
