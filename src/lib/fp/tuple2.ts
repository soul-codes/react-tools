export type Tuple2<A, B> = readonly [A, B];

export const of = <A, B>(a: A, b: B): Tuple2<A, B> => [a, b];

export const bimap = <A1, A2, B1, B2>(
  mapLeft: (a: A1) => A2,
  mapRight: (b: B1) => B2
) => (m: Tuple2<A1, B1>): Tuple2<A2, B2> => [mapLeft(m[0]), mapRight(m[1])];

export const map0 = <A1, A2>(map: (a: A1) => A2) => <B>(
  m: Tuple2<A1, B>
): Tuple2<A2, B> => [map(m[0]), m[1]];

export const map1 = <B1, B2>(map: (b: B1) => B2) => <A>(
  m: Tuple2<A, B1>
): Tuple2<A, B2> => [m[0], map(m[1])];
