export type Maybe<T> = Boxed<T> | null | undefined;

export interface Boxed<T> {
  value: T;
}

export const box = <T>(value: T): Boxed<T> => ({ value });

export const empty = <T>(): Maybe<T> => null;

export const getOrElse = <T>(fallback: () => T) => (maybe: Maybe<T>) =>
  maybe ? maybe.value : fallback();

export const getOrElseW = <T>(fallback: () => T) => <U>(maybe: Maybe<U>) =>
  maybe ? maybe.value : fallback();

export const getOrElseC = <T>(fallback: T) => (maybe: Maybe<T>) =>
  maybe ? maybe.value : fallback;

export const getOrElseCW = <T>(fallback: T) => <U>(maybe: Maybe<U>) =>
  maybe ? maybe.value : fallback;

export const preCompare = <T>(
  a: Maybe<T>,
  b: Maybe<T>,
  next: (a: T, b: T) => boolean
) =>
  a != null && b != null ? next(a.value, b.value) : (a == null) === (b == null);
