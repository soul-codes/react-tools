export interface Lazy<T> {
  (): T;
}

export const get = <T>(lazy: Lazy<T>) => lazy();
