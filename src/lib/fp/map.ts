export interface Map<A, B> {
  (a: A): B;
}

export const call = <A>(value: A) => <B>(map: Map<A, B>) => map(value);
