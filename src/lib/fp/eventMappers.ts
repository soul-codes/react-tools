import { ChangeEvent } from "react";

/**
 * Creates a function that extracts a `ChangeEvent`'s target `value`.
 */
export const changeValue = <T>() => (ev: ChangeEvent<{ value: T }>) =>
  ev.target.value;

/**
 * Extracts a `ChangeEvent`'s target `checked` property.
 */
export const changeChecked = (ev: ChangeEvent<{ checked: boolean }>) =>
  ev.target.checked;
