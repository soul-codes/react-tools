/**
 * Defines a recursive function given the type of the resulting function. The
 * recursor is then given as the first argument, and the remaining arguments
 * are shifted.
 * @param fn
 */
export function recursive<A extends any[], B>(
  fn: (next: Function<A, B>, ...args: A) => B
): Function<A, B> {
  const recurse = (...args: A): B => fn(recurse, ...args);
  return recurse;
}

export interface Function<A extends any[], B> {
  (...args: A): B;
}

export type NonFunction<T> = Exclude<T, Function<any, any>>;
