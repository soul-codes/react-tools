import { Map } from "./map";

export type Dispatch<T> = (payload: T) => void;

export const map = <A, B>(fn: Map<B, A>) => (m: Dispatch<A>): Dispatch<B> => (
  payload
) => m(fn(payload));

export const call = <T>(value: T) => (m: Dispatch<T>) => m(value);
