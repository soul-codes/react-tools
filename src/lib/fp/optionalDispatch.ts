import { Dispatch } from "react";

import { Map } from "./map";

export type OptionalDispatch<T> = Dispatch<T> | undefined;

export const map = <A, B>(fn: Map<B, A>) => (
  m: OptionalDispatch<A>
): OptionalDispatch<B> => (payload) => m?.(fn(payload));

export const call = <T>(value: T) => (m: OptionalDispatch<T>) => m?.(value);
