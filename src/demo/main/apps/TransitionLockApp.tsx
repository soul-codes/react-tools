import { interval } from "@lib/effects/interval";
import { pipe } from "@lib/fp/pipe";
import * as t from "@lib/fp/task";
import { useInit } from "@lib/hooks";
import * as sh from "@lib/hooks-fp/state";
import { collect } from "@lib/patterns/collect";
import { transitionCoordinator } from "@lib/patterns/transitionCoordinator";
import React, { useEffect } from "react";

const coordinator = transitionCoordinator();
const lockCount = collect(0);

export function TransitionLockApp() {
  const suspend = useInit(() =>
    pipe(
      () =>
        new Promise((resolve) =>
          setTimeout(resolve, Math.random() * 1000 + 2000)
        ),
      t.wrap(() => lockCount.reduceEffect((n) => ++n)),
      t.wrap(coordinator.lock)
    )
  );

  return (
    <div>
      <button onClick={suspend}>Suspend</button>
      <LockCount />
      <Timer />
      <Timer />
      <Timer coordinated />
      <Timer coordinated />
    </div>
  );
}

function Timer(props: { coordinated?: boolean }) {
  const freq = useInit(() => Math.random() * 500 + 250);
  const [counter, incr] = pipe(
    sh.of<number>(),
    sh.mapReducer((_: void) => (num) => ++num),
    sh.use(0)
  );
  useEffect(() => interval(incr, freq));
  return (
    <div>
      {props.coordinated
        ? coordinator.useCoordinatedTransition(counter)
        : counter}
      {props.coordinated && " (coordinated)"}
    </div>
  );
}

function LockCount() {
  return <div>Lock count: {lockCount.useValue()}</div>;
}
