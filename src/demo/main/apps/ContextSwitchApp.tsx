import { pipe } from "@lib/fp/pipe";
import * as sh from "@lib/hooks-fp/state";
import React, { createContext, useContext, useEffect } from "react";

const contextA = createContext("a");
const contextB = createContext("b");
type AB = "a" | "b";

/**
 * This app verifies that React useContext hooks can take different contexts
 * at different render times.
 */
export function ContextSwitchApp() {
  const [ab, invert] = pipe(
    sh.of<AB>(),
    sh.mapReducer((_: void) => (value) => (value === "a" ? "b" : "a"))
  )("a");

  useEffect(() => {
    const interval = setInterval(invert, 1000);
    return () => clearInterval(interval);
  }, []);

  return <div>{ab === "a" ? useContext(contextA) : useContext(contextB)}</div>;
}
