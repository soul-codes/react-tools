import React from "react";

/**
 * This app verifies that React's element ref will be called with null on
 * render
 */
export function RefApp() {
  return <div ref={(el) => console.log("ref", el)}>Check Console Log</div>;
}
