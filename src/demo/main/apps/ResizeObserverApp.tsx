import { pipe } from "@lib/fp/pipe";
import { useResizeObserverRef } from "@lib/hooks";
import * as sh from "@lib/hooks-fp/state";
import React from "react";

export function ResizeObserverApp() {
  const [size, setSize] = pipe(
    sh.of<ClientRect | null>(),
    sh.structDispatch(),
    sh.mapDispatch((el: HTMLDivElement) => el.getBoundingClientRect()),
    sh.use(null)
  );

  return (
    <div>
      <div ref={useResizeObserverRef(setSize)}></div>
      <div>{JSON.stringify(size, null, 2)}</div>
    </div>
  );
}
