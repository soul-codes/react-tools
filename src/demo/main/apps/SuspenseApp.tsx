import { suspense } from "@lib/patterns/suspense";
import React, { Fragment, useEffect, useState } from "react";

export function SuspenseApp() {
  const [suspenseState, Suspense] = LoadingErrorSuspense.useSuspense();
  return (
    <Suspense
      mask={
        suspenseState.loading && (
          <Fragment>
            <div hidden={!suspenseState.error}>App: We have a problem</div>
            <div hidden={!suspenseState.loading}>App: Loading</div>
          </Fragment>
        )
      }
    >
      <Loader />
      <Loader />
      <Loader />
      <SuperLoader />
    </Suspense>
  );
}

function Loader() {
  const [status, setStatus] = useState<SuspenseMetaState>("loading");

  useEffect(() => {
    const timeout = setTimeout(
      () => setStatus(Math.random() < 0.4 ? "error" : "ready"),
      Math.random() * 4000
    );
    return () => clearTimeout(timeout);
  }, []);

  LoadingErrorSuspense.useReport(status);
  return <div>Hello {status}</div>;
}

function SuperLoader() {
  const [suspenseState, Suspense] = LoadingErrorSuspense.useSuspense();

  return (
    <div style={{ border: "1px solid red" }}>
      <Suspense>
        mask=
        {suspenseState.loading && (
          <Fragment>
            <div hidden={!suspenseState.error}>
              SuperLoader: We have a problem
            </div>
            <div hidden={!suspenseState.loading}>SuperLoader: Loading</div>
          </Fragment>
        )}
        <div hidden={suspenseState.loading}>
          <Loader />
          <Loader />
          <Loader />
        </div>
      </Suspense>
    </div>
  );
}

type SuspenseMetaState = "loading" | "error" | "ready";

interface PresentMetaState {
  loading: boolean;
  error: boolean;
}

const LoadingErrorSuspense = suspense(
  (states: (SuspenseMetaState | PresentMetaState)[]): PresentMetaState => ({
    loading: states.some((state) =>
      typeof state === "object" ? state.loading : state === "loading"
    ),
    error: states.some((state) =>
      typeof state === "object" ? state.error : state === "error"
    ),
  })
);
