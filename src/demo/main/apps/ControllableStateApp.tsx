import * as dp from "@lib/fp/dispatch";
import * as evm from "@lib/fp/eventMappers";
import * as odp from "@lib/fp/optionalDispatch";
import { useInit } from "@lib/hooks";
import * as sh from "@lib/hooks-fp/state";
import { pipe } from "fp-ts/lib/function.js";
import React from "react";

export function ControllableStateApp() {
  const [isControlled, setIsControlled] = pipe(sh.of<boolean>(), sh.use(false));
  const [isDispatched, setIsDispatched] = pipe(sh.of<boolean>(), sh.use(false));
  const [value, setValue] = pipe(sh.of<string>(), sh.use(""));

  return (
    <div>
      <Checkbox
        label={"Controlled"}
        checked={isControlled}
        onCheckedChange={setIsControlled}
      />
      <Checkbox
        label={"Dispatch"}
        checked={isDispatched}
        onCheckedChange={setIsDispatched}
      />
      <Input
        value={isControlled ? value : void 0}
        onValueChange={isDispatched ? setValue : void 0}
      />
      <div>Outer state: {value}</div>
    </div>
  );
}

function Checkbox(
  props: sh.ControllableState<{ checked: boolean }> & { label: string }
) {
  return (
    <div>
      {props.label}{" "}
      <input
        type="checkbox"
        onChange={useInit(() =>
          pipe(props.onCheckedChange, odp.map(evm.changeChecked))
        )}
      />
    </div>
  );
}

function Input(
  props: sh.ControllableState<{ value: string; isOpen: boolean }>
) {
  const [value, setValue] = sh.controllable(
    props.value,
    props.onValueChange
  )("");

  return (
    <div>
      <input
        value={value}
        onChange={useInit(() => pipe(setValue, dp.map(evm.changeValue())))}
      />
      <button type="button" onClick={useInit(() => () => setValue(""))}>
        clear
      </button>
      Inner state: {value}
    </div>
  );
}
