import { pipe } from "@lib/fp/pipe";
import * as ctx from "@lib/hooks-fp/context";
import React, { createContext } from "react";

const contextA = createContext("a");
const contextB = createContext("b");

const useContextAB = pipe(
  ctx.sequenceS({ a: ctx.of(contextA), b: ctx.of(contextB) }),
  ctx.map((ab) => ab.a + ":" + ab.b)
);

/**
 * This app verifies that React useContext hooks can take different contexts
 * at different render times.
 */
export function DerivedContextApp() {
  const ab = useContextAB();
  return <div>{ab}</div>;
}
