import { RenderRate } from "@lib/components/RenderRate";
import { map } from "@lib/fp/dispatch";
import { Collect, collect } from "@lib/patterns/collect";
import { pipe } from "fp-ts/lib/function.js";
import React, { useRef, useState } from "react";

export function CollectApp() {
  const { current: abc } = useRef(collect({ a: "", b: "", c: "" }));
  return (
    <div>
      <Display abc={abc} />
      <DisplayA abc={abc} />
      <Input abc={abc} k="a" />
      <Input abc={abc} k="b" />
      <Input2 abc={abc} k="c" />
    </div>
  );
}

function Input(props: { k: "a" | "b" | "c"; abc: Collect<ABC> }) {
  console.log("render", props.k);
  const [val, setVal] = useState("");
  props.abc.useReducer((abc) => ({ ...abc, [props.k]: val }));

  return <input value={val} onChange={(ev) => setVal(ev.target.value)} />;
}

function Input2(props: { k: "a" | "b" | "c"; abc: Collect<ABC> }) {
  return (
    <input
      value={props.abc.useProjection((abc) => abc[props.k])}
      onChange={pipe(
        props.abc.useReducerRef((val: string) => (abc) => ({
          ...abc,
          [props.k]: val,
        })),
        map((ev) => ev.target.value)
      )}
    />
  );
}

function Display(props: { abc: Collect<ABC> }) {
  return <div>{JSON.stringify(props.abc.useValue(), null, 2)}</div>;
}

function DisplayA(props: { abc: Collect<ABC> }) {
  console.log("render display A");
  return (
    <div>
      <RenderRate />
      {JSON.stringify(
        props.abc.useProjection(({ a }) => a),
        null,
        2
      )}
    </div>
  );
}

interface ABC {
  a: string;
  b: string;
  c: string;
}
