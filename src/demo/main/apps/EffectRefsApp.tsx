import { RenderRate } from "@lib/components/RenderRate";
import { interval } from "@lib/effects/interval";
import { pipe } from "@lib/fp/pipe";
import { useEffectRefs, useInit } from "@lib/hooks";
import * as sh from "@lib/hooks-fp/state";
import { Collect, collect } from "@lib/patterns/collect";
import React, { PropsWithChildren, forwardRef, useEffect } from "react";

/**
 * This app verifies that React useContext hooks can take different contexts
 * at different render times.
 */
export function EffectRefsApp() {
  const countState = useInit(() => collect(0));

  const ref = countState.useReducerRef((count: number) => () => count);
  const [
    a,
    b,
    c,
  ] = useEffectRefs(
    (
      a: HTMLDivElement | null,
      b: HTMLDivElement | null,
      c: HTMLDivElement | null
    ) => ref([a, b, c].filter(Boolean).length)
  );

  return (
    <div>
      <Display countState={countState} />
      <Flicker ref={a}>1</Flicker>
      <Flicker ref={b}>2</Flicker>
      <Flicker ref={c}>3</Flicker>
    </div>
  );
}

const Flicker = forwardRef<HTMLDivElement, PropsWithChildren<{}>>(
  (props, ref) => {
    const [is, invert] = pipe(
      sh.of<boolean>(),
      sh.mapReducer((_: void) => (value) => !value)
    )(true);

    useEffect(() => interval(invert, (Math.random() + 1) * 1000));
    return is ? (
      <div ref={ref}>
        <RenderRate />
        {props.children}
      </div>
    ) : null;
  }
);

const Display = (props: { countState: Collect<number> }) => {
  return <div>Count: {props.countState.useValue()}</div>;
};
