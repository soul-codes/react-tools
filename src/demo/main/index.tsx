import * as app from "apps";
import { pipe } from "fp-ts/lib/function";
import React, { ComponentType, useState } from "react";
import { render } from "react-dom";

const entries = Object.entries(app);
const components = new Map<string, ComponentType<{}>>(entries);

const el = document.createElement("div");
document.body.appendChild(el);
render(<App />, el);

function App() {
  const [app, setApp] = useState(entries[0][0]);
  return (
    <div>
      <select onChange={(el) => setApp(el.target.value)} value={app}>
        {[...components.keys()].sort().map((val) => (
          <option value={val}>{val}</option>
        ))}
      </select>
      <div>{pipe(components.get(app) ?? null, (C) => C && <C />)}</div>
    </div>
  );
}
