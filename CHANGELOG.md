* chore: 🤖 es friendly (eeb2941)
* feat: 🎸 state instance (71aaf8c)
* fix: 🐛 correct the spelling of "referentialComparer" (4847d30)

* chore: 🤖 alternative webpack dev server port (67630a6)
* feat: 🎸 controllable state hooks (f0566d0)
* fix: 🐛 allow useInit() to accept a constant (239fb5a)

* fix: 🐛 fix bugs in useEffectRefs, rename types (4bf1414)
* feat: 🎸 useLeadingRef hook (6b45144)

* chore: 🤖 properly generate source map paths for lib (6fe59ef)
* fix: 🐛 RenderRate bug (346befe)

* chore: 🤖 update dependencies to have sourcemap (ffb8317)
* feat: 🎸 transition coordinator (9f088c0)

* chore: 🤖 package is public (864ef72)

# 0.1.0 (2021-02-11)

